
desc "This task is called by the Heroku scheduler add-on"



task :deposit_check => :environment  do
  coinbase = Coinbase::Client.new('d73bb8a4131d5df2fef7e48afc0812f97cb1297fb60ab6244238cfce51bb5f4f')
  c = coinbase.get('/orders').to_hash

  (0..100).each do |counter|
    begin
      #pay_info = c["orders"][1]["order"]
      pay_info = c["orders"][counter]["order"]
      btc_cent = 100000000
      hash_adress = pay_info["transaction"]["hash"]
      response = Net::HTTP.get_response("blockchain.info", "/ru/tx/#{hash_adress}?format=json")
      wallet = JSON.parse(response.body)
      sender_wallet = wallet["inputs"][0]["prev_out"]["addr"]
      transaction_id = pay_info["id"]
      chance_cents = pay_info["total_btc"]["cents"]

      money_dep = chance_cents.to_f /  btc_cent.to_f


      deposit = Deposit.create(trans_id: transaction_id, money: money_dep, approve: false )

      if deposit.save
        user = User.find_by_wallet(sender_wallet)
        new_balance = user.balance + money_dep
        user.balance = new_balance
        user.save!
        deposit.approve = true
        deposit.save!
      end



    rescue
    end
  end





end



task :play_zero_size => :environment do

  bets_zero = Bet.where(price: 0)



  while bets_zero.map(&:wallet).uniq.size >= 2
    bets_zero = Bet.where(price: 0)
    uniq_wallets = bets_zero.map(&:wallet).uniq
    uniq_wallets_size = bets_zero.map(&:wallet).uniq.size
    uniq_size = uniq_wallets_size -= 1
    puts uniq_wallets_size
    left_bet =  Bet.where(wallet: uniq_wallets[0]).first
    puts left_bet.wallet

    right_bet = Bet.where(wallet: uniq_wallets[rand(1..uniq_size)]).first
    puts right_bet.wallet
    r_paper =  left_bet.motion == "rock" && right_bet.motion == "paper"
    r_scissors = left_bet.motion == "paper" && right_bet.motion == "scissors"
    r_rock =  left_bet.motion == "scissors" && right_bet.motion == "rock"
    l_paper = left_bet.motion == "paper" && right_bet.motion == "rock"
    l_scissors =  left_bet.motion == "scissors" && right_bet.motion == "paper"
    l_rock =  left_bet.motion == "rock" && right_bet.motion == "scissors"
    case
      when r_paper
        Game.create(left_wallet: left_bet.wallet, right_wallet: right_bet.wallet, left_motion: left_bet.motion, right_motion: right_bet.motion, win_wallet: right_bet.wallet, lose_wallet: left_bet.wallet, left_stat: "lose", right_stat:"win", win_money: 0.0000199)
        right_user = User.find_by_wallet(right_bet.wallet)
        right_user.balance += 0.0000199
        right_user.balance = right_user.balance
        right_user.save!
      when  r_scissors
        Game.create(left_wallet: left_bet.wallet, right_wallet: right_bet.wallet, left_motion: left_bet.motion, right_motion: right_bet.motion, win_wallet: right_bet.wallet, lose_wallet: left_bet.wallet, left_stat: "lose", right_stat:"win", win_money: 0.0000199)
        right_user = User.find_by_wallet(right_bet.wallet)
        right_user.balance += 0.0000199
        right_user.balance = right_user.balance
        right_user.save!
      when  r_rock
        Game.create(left_wallet: left_bet.wallet, right_wallet: right_bet.wallet, left_motion: left_bet.motion, right_motion: right_bet.motion, win_wallet: right_bet.wallet, lose_wallet: left_bet.wallet, left_stat: "lose", right_stat:"win", win_money: 0.0000199)
        right_user = User.find_by_wallet(right_bet.wallet)
        right_user.balance += 0.0000199
        right_user.balance = right_user.balance
        right_user.save!
      when l_paper
        Game.create(left_wallet: left_bet.wallet, right_wallet: right_bet.wallet, left_motion: left_bet.motion, right_motion: right_bet.motion, win_wallet: left_bet.wallet, lose_wallet: right_bet.wallet, left_stat: "win", right_stat:"lose", win_money: 0.0000199)
        left_user = User.find_by_wallet(left_bet.wallet)
        left_user.balance += 0.0000199
        left_user.balance = left_user.balance
        left_user.save!
      when l_scissors
        Game.create(left_wallet: left_bet.wallet, right_wallet: right_bet.wallet, left_motion: left_bet.motion, right_motion: right_bet.motion, win_wallet: left_bet.wallet, lose_wallet: right_bet.wallet, left_stat: "win", right_stat:"lose", win_money: 0.0000199)
        left_user = User.find_by_wallet(left_bet.wallet)
        left_user.balance += 0.0000199
        left_user.balance = left_user.balance
        left_user.save!
      when l_rock
        Game.create(left_wallet: left_bet.wallet, right_wallet: right_bet.wallet, left_motion: left_bet.motion, right_motion: right_bet.motion, win_wallet: left_bet.wallet, lose_wallet: right_bet.wallet, left_stat: "win", right_stat:"lose", win_money: 0.0000199)
        left_user = User.find_by_wallet(left_bet.wallet)
        left_user.balance += 0.0000199
        left_user.balance = left_user.balance
        left_user.save!
      when   left_bet.motion == right_bet.motion
        Game.create(left_wallet: left_bet.wallet, right_wallet: right_bet.wallet, left_motion: left_bet.motion, right_motion: right_bet.motion, win_wallet: nil, lose_wallet: nil, left_stat: "draw", right_stat:"draw", win_money: 0)
        left_user = User.find_by_wallet(left_bet.wallet)
        right_user = User.find_by_wallet(right_bet.wallet)
        left_user.balance += 0.00001
        left_user.save!
        right_user.balance += 0.00001
        right_user.save!
    end

    left_bet.destroy
    right_bet.destroy
  end


end


task :play_one_size => :environment do

  bets_one = Bet.where(price: 1)



  while bets_one.map(&:wallet).uniq.size >= 2
    bets_one = Bet.where(price: 1)
    uniq_wallets = bets_one.map(&:wallet).uniq
    uniq_wallets_size = bets_one.map(&:wallet).uniq.size
    uniq_size = uniq_wallets_size -= 1
    puts uniq_wallets_size
    left_bet =  Bet.where(wallet: uniq_wallets[0]).first
    puts left_bet.wallet

    right_bet = Bet.where(wallet: uniq_wallets[rand(1..uniq_size)]).first
    puts right_bet.wallet
    r_paper =  left_bet.motion == "rock" && right_bet.motion == "paper"
    r_scissors = left_bet.motion == "paper" && right_bet.motion == "scissors"
    r_rock =  left_bet.motion == "scissors" && right_bet.motion == "rock"
    l_paper = left_bet.motion == "paper" && right_bet.motion == "rock"
    l_scissors =  left_bet.motion == "scissors" && right_bet.motion == "paper"
    l_rock =  left_bet.motion == "rock" && right_bet.motion == "scissors"
    case
      when r_paper
        Game.create(left_wallet: left_bet.wallet, right_wallet: right_bet.wallet, left_motion: left_bet.motion, right_motion: right_bet.motion, win_wallet: right_bet.wallet, lose_wallet: left_bet.wallet, left_stat: "lose", right_stat:"win", win_money: 0.000199)
        right_user = User.find_by_wallet(right_bet.wallet)
        right_user.balance += 0.000199
        right_user.balance = right_user.balance
        right_user.save!
      when  r_scissors
        Game.create(left_wallet: left_bet.wallet, right_wallet: right_bet.wallet, left_motion: left_bet.motion, right_motion: right_bet.motion, win_wallet: right_bet.wallet, lose_wallet: left_bet.wallet, left_stat: "lose", right_stat:"win", win_money: 0.000199)
        right_user = User.find_by_wallet(right_bet.wallet)
        right_user.balance += 0.000199
        right_user.balance = right_user.balance
        right_user.save!
      when  r_rock
        Game.create(left_wallet: left_bet.wallet, right_wallet: right_bet.wallet, left_motion: left_bet.motion, right_motion: right_bet.motion, win_wallet: right_bet.wallet, lose_wallet: left_bet.wallet, left_stat: "lose", right_stat:"win", win_money: 0.000199)
        right_user = User.find_by_wallet(right_bet.wallet)
        right_user.balance += 0.000199
        right_user.balance = right_user.balance
        right_user.save!
      when l_paper
        Game.create(left_wallet: left_bet.wallet, right_wallet: right_bet.wallet, left_motion: left_bet.motion, right_motion: right_bet.motion, win_wallet: left_bet.wallet, lose_wallet: right_bet.wallet, left_stat: "win", right_stat:"lose", win_money: 0.000199)
        left_user = User.find_by_wallet(left_bet.wallet)
        left_user.balance += 0.000199
        left_user.balance = left_user.balance
        left_user.save!
      when l_scissors
        Game.create(left_wallet: left_bet.wallet, right_wallet: right_bet.wallet, left_motion: left_bet.motion, right_motion: right_bet.motion, win_wallet: left_bet.wallet, lose_wallet: right_bet.wallet, left_stat: "win", right_stat:"lose", win_money: 0.000199)
        left_user = User.find_by_wallet(left_bet.wallet)
        left_user.balance += 0.000199
        left_user.balance = left_user.balance
        left_user.save!
      when l_rock
        Game.create(left_wallet: left_bet.wallet, right_wallet: right_bet.wallet, left_motion: left_bet.motion, right_motion: right_bet.motion, win_wallet: left_bet.wallet, lose_wallet: right_bet.wallet, left_stat: "win", right_stat:"lose", win_money: 0.000199)
        left_user = User.find_by_wallet(left_bet.wallet)
        left_user.balance += 0.000199
        left_user.balance = left_user.balance
        left_user.save!
      when   left_bet.motion == right_bet.motion
        Game.create(left_wallet: left_bet.wallet, right_wallet: right_bet.wallet, left_motion: left_bet.motion, right_motion: right_bet.motion, win_wallet: nil, lose_wallet: nil, left_stat: "draw", right_stat:"draw", win_money: 0)
        left_user = User.find_by_wallet(left_bet.wallet)
        right_user = User.find_by_wallet(right_bet.wallet)
        left_user.balance += 0.0001
        left_user.save!
        right_user.balance += 0.0001
        right_user.save!
    end

    left_bet.destroy
    right_bet.destroy
  end


end

task :play_two_size => :environment do


  bets_two = Bet.where(price: 2)



  while bets_two.map(&:wallet).uniq.size >= 2
    bets_two = Bet.where(price: 2)
    uniq_wallets = bets_two.map(&:wallet).uniq
    uniq_wallets_size = bets_two.map(&:wallet).uniq.size
    uniq_size = uniq_wallets_size -= 1
    puts uniq_wallets_size
    left_bet =  Bet.where(wallet: uniq_wallets[0]).first
    puts left_bet.wallet

    right_bet = Bet.where(wallet: uniq_wallets[rand(1..uniq_size)]).first
    puts right_bet.wallet
    r_paper =  left_bet.motion == "rock" && right_bet.motion == "paper"
    r_scissors = left_bet.motion == "paper" && right_bet.motion == "scissors"
    r_rock =  left_bet.motion == "scissors" && right_bet.motion == "rock"
    l_paper = left_bet.motion == "paper" && right_bet.motion == "rock"
    l_scissors =  left_bet.motion == "scissors" && right_bet.motion == "paper"
    l_rock =  left_bet.motion == "rock" && right_bet.motion == "scissors"
    case
      when r_paper
        Game.create(left_wallet: left_bet.wallet, right_wallet: right_bet.wallet, left_motion: left_bet.motion, right_motion: right_bet.motion, win_wallet: right_bet.wallet, lose_wallet: left_bet.wallet, left_stat: "lose", right_stat:"win", win_money: 0.00199)
        right_user = User.find_by_wallet(right_bet.wallet)
        right_user.balance += 0.00199
        right_user.balance = right_user.balance
        right_user.save!
      when  r_scissors
        Game.create(left_wallet: left_bet.wallet, right_wallet: right_bet.wallet, left_motion: left_bet.motion, right_motion: right_bet.motion, win_wallet: right_bet.wallet, lose_wallet: left_bet.wallet, left_stat: "lose", right_stat:"win", win_money: 0.00199)
        right_user = User.find_by_wallet(right_bet.wallet)
        right_user.balance += 0.00199
        right_user.balance = right_user.balance
        right_user.save!
      when  r_rock
        Game.create(left_wallet: left_bet.wallet, right_wallet: right_bet.wallet, left_motion: left_bet.motion, right_motion: right_bet.motion, win_wallet: right_bet.wallet, lose_wallet: left_bet.wallet, left_stat: "lose", right_stat:"win", win_money: 0.00199)
        right_user = User.find_by_wallet(right_bet.wallet)
        right_user.balance += 0.00199
        right_user.balance = right_user.balance
        right_user.save!
      when l_paper
        Game.create(left_wallet: left_bet.wallet, right_wallet: right_bet.wallet, left_motion: left_bet.motion, right_motion: right_bet.motion, win_wallet: left_bet.wallet, lose_wallet: right_bet.wallet, left_stat: "win", right_stat:"lose", win_money: 0.00199)
        left_user = User.find_by_wallet(left_bet.wallet)
        left_user.balance += 0.00199
        left_user.balance = left_user.balance
        left_user.save!
      when l_scissors
        Game.create(left_wallet: left_bet.wallet, right_wallet: right_bet.wallet, left_motion: left_bet.motion, right_motion: right_bet.motion, win_wallet: left_bet.wallet, lose_wallet: right_bet.wallet, left_stat: "win", right_stat:"lose", win_money: 0.00199)
        left_user = User.find_by_wallet(left_bet.wallet)
        left_user.balance += 0.00199
        left_user.balance = left_user.balance
        left_user.save!
      when l_rock
        Game.create(left_wallet: left_bet.wallet, right_wallet: right_bet.wallet, left_motion: left_bet.motion, right_motion: right_bet.motion, win_wallet: left_bet.wallet, lose_wallet: right_bet.wallet, left_stat: "win", right_stat:"lose", win_money: 0.00199)
        left_user = User.find_by_wallet(left_bet.wallet)
        left_user.balance += 0.00199
        left_user.balance = left_user.balance
        left_user.save!
      when   left_bet.motion == right_bet.motion
        Game.create(left_wallet: left_bet.wallet, right_wallet: right_bet.wallet, left_motion: left_bet.motion, right_motion: right_bet.motion, win_wallet: nil, lose_wallet: nil, left_stat: "draw", right_stat:"draw", win_money: 0)
        left_user = User.find_by_wallet(left_bet.wallet)
        right_user = User.find_by_wallet(right_bet.wallet)
        left_user.balance += 0.001
        left_user.save!
        right_user.balance += 0.001
        right_user.save!
    end


    left_bet.destroy
    right_bet.destroy
  end


end

task :play_three_size => :environment do


  bets_three = Bet.where(price: 3)



  while bets_three.map(&:wallet).uniq.size >= 2
    bets_three = Bet.where(price: 3)
    uniq_wallets = bets_three.map(&:wallet).uniq
    uniq_wallets_size = bets_three.map(&:wallet).uniq.size
    uniq_size = uniq_wallets_size -= 1
    puts uniq_wallets_size
    left_bet =  Bet.where(wallet: uniq_wallets[0]).first
    puts left_bet.wallet

    right_bet = Bet.where(wallet: uniq_wallets[rand(1..uniq_size)]).first
    puts right_bet.wallet
    r_paper =  left_bet.motion == "rock" && right_bet.motion == "paper"
    r_scissors = left_bet.motion == "paper" && right_bet.motion == "scissors"
    r_rock =  left_bet.motion == "scissors" && right_bet.motion == "rock"
    l_paper = left_bet.motion == "paper" && right_bet.motion == "rock"
    l_scissors =  left_bet.motion == "scissors" && right_bet.motion == "paper"
    l_rock =  left_bet.motion == "rock" && right_bet.motion == "scissors"
    case
      when r_paper
        Game.create(left_wallet: left_bet.wallet, right_wallet: right_bet.wallet, left_motion: left_bet.motion, right_motion: right_bet.motion, win_wallet: right_bet.wallet, lose_wallet: left_bet.wallet, left_stat: "lose", right_stat:"win", win_money: 0.0199)
        right_user = User.find_by_wallet(right_bet.wallet)
        right_user.balance += 0.0199
        right_user.balance = right_user.balance
        right_user.save!
      when  r_scissors
        Game.create(left_wallet: left_bet.wallet, right_wallet: right_bet.wallet, left_motion: left_bet.motion, right_motion: right_bet.motion, win_wallet: right_bet.wallet, lose_wallet: left_bet.wallet, left_stat: "lose", right_stat:"win", win_money: 0.0199)
        right_user = User.find_by_wallet(right_bet.wallet)
        right_user.balance += 0.0199
        right_user.balance = right_user.balance
        right_user.save!
      when  r_rock
        Game.create(left_wallet: left_bet.wallet, right_wallet: right_bet.wallet, left_motion: left_bet.motion, right_motion: right_bet.motion, win_wallet: right_bet.wallet, lose_wallet: left_bet.wallet, left_stat: "lose", right_stat:"win", win_money: 0.0199)
        right_user = User.find_by_wallet(right_bet.wallet)
        right_user.balance += 0.0199
        right_user.balance = right_user.balance
        right_user.save!
      when l_paper
        Game.create(left_wallet: left_bet.wallet, right_wallet: right_bet.wallet, left_motion: left_bet.motion, right_motion: right_bet.motion, win_wallet: left_bet.wallet, lose_wallet: right_bet.wallet, left_stat: "win", right_stat:"lose", win_money: 0.0199)
        left_user = User.find_by_wallet(left_bet.wallet)
        left_user.balance += 0.0199
        left_user.balance = left_user.balance
        left_user.save!
      when l_scissors
        Game.create(left_wallet: left_bet.wallet, right_wallet: right_bet.wallet, left_motion: left_bet.motion, right_motion: right_bet.motion, win_wallet: left_bet.wallet, lose_wallet: right_bet.wallet, left_stat: "win", right_stat:"lose", win_money: 0.0199)
        left_user = User.find_by_wallet(left_bet.wallet)
        left_user.balance += 0.0199
        left_user.balance = left_user.balance
        left_user.save!
      when l_rock
        Game.create(left_wallet: left_bet.wallet, right_wallet: right_bet.wallet, left_motion: left_bet.motion, right_motion: right_bet.motion, win_wallet: left_bet.wallet, lose_wallet: right_bet.wallet, left_stat: "win", right_stat:"lose", win_money: 0.0199)
        left_user = User.find_by_wallet(left_bet.wallet)
        left_user.balance += 0.0199
        left_user.balance = left_user.balance
        left_user.save!
      when   left_bet.motion == right_bet.motion
        Game.create(left_wallet: left_bet.wallet, right_wallet: right_bet.wallet, left_motion: left_bet.motion, right_motion: right_bet.motion, win_wallet: nil, lose_wallet: nil, left_stat: "draw", right_stat:"draw", win_money: 0)
        left_user = User.find_by_wallet(left_bet.wallet)
        right_user = User.find_by_wallet(right_bet.wallet)
        left_user.balance += 0.01
        left_user.save!
        right_user.balance += 0.01
        right_user.save!
    end

    left_bet.destroy
    right_bet.destroy
  end


end

task :play_four_size => :environment do


  bets_four = Bet.where(price: 4)



  while bets_four.map(&:wallet).uniq.size >= 2
    bets_four = Bet.where(price: 4)
    uniq_wallets = bets_four.map(&:wallet).uniq
    uniq_wallets_size = bets_four.map(&:wallet).uniq.size
    uniq_size = uniq_wallets_size -= 1
    puts uniq_wallets_size
    left_bet =  Bet.where(wallet: uniq_wallets[0]).first
    puts left_bet.wallet

    right_bet = Bet.where(wallet: uniq_wallets[rand(1..uniq_size)]).first
    puts right_bet.wallet
    r_paper =  left_bet.motion == "rock" && right_bet.motion == "paper"
    r_scissors = left_bet.motion == "paper" && right_bet.motion == "scissors"
    r_rock =  left_bet.motion == "scissors" && right_bet.motion == "rock"
    l_paper = left_bet.motion == "paper" && right_bet.motion == "rock"
    l_scissors =  left_bet.motion == "scissors" && right_bet.motion == "paper"
    l_rock =  left_bet.motion == "rock" && right_bet.motion == "scissors"
    case
      when r_paper
        Game.create(left_wallet: left_bet.wallet, right_wallet: right_bet.wallet, left_motion: left_bet.motion, right_motion: right_bet.motion, win_wallet: right_bet.wallet, lose_wallet: left_bet.wallet, left_stat: "lose", right_stat:"win", win_money: 0.199)
        right_user = User.find_by_wallet(right_bet.wallet)
        right_user.balance += 0.199
        right_user.balance = right_user.balance
        right_user.save!
      when  r_scissors
        Game.create(left_wallet: left_bet.wallet, right_wallet: right_bet.wallet, left_motion: left_bet.motion, right_motion: right_bet.motion, win_wallet: right_bet.wallet, lose_wallet: left_bet.wallet, left_stat: "lose", right_stat:"win", win_money: 0.199)
        right_user = User.find_by_wallet(right_bet.wallet)
        right_user.balance += 0.199
        right_user.balance = right_user.balance
        right_user.save!
      when  r_rock
        Game.create(left_wallet: left_bet.wallet, right_wallet: right_bet.wallet, left_motion: left_bet.motion, right_motion: right_bet.motion, win_wallet: right_bet.wallet, lose_wallet: left_bet.wallet, left_stat: "lose", right_stat:"win", win_money: 0.199)
        right_user = User.find_by_wallet(right_bet.wallet)
        right_user.balance += 0.199
        right_user.balance = right_user.balance
        right_user.save!
      when l_paper
        Game.create(left_wallet: left_bet.wallet, right_wallet: right_bet.wallet, left_motion: left_bet.motion, right_motion: right_bet.motion, win_wallet: left_bet.wallet, lose_wallet: right_bet.wallet, left_stat: "win", right_stat:"lose", win_money: 0.199)
        left_user = User.find_by_wallet(left_bet.wallet)
        left_user.balance += 0.199
        left_user.balance = left_user.balance
        left_user.save!
      when l_scissors
        Game.create(left_wallet: left_bet.wallet, right_wallet: right_bet.wallet, left_motion: left_bet.motion, right_motion: right_bet.motion, win_wallet: left_bet.wallet, lose_wallet: right_bet.wallet, left_stat: "win", right_stat:"lose", win_money: 0.199)
        left_user = User.find_by_wallet(left_bet.wallet)
        left_user.balance += 0.199
        left_user.balance = left_user.balance
        left_user.save!
      when l_rock
        Game.create(left_wallet: left_bet.wallet, right_wallet: right_bet.wallet, left_motion: left_bet.motion, right_motion: right_bet.motion, win_wallet: left_bet.wallet, lose_wallet: right_bet.wallet, left_stat: "win", right_stat:"lose", win_money: 0.199)
        left_user = User.find_by_wallet(left_bet.wallet)
        left_user.balance += 0.199
        left_user.balance = left_user.balance
        left_user.save!
      when   left_bet.motion == right_bet.motion
        Game.create(left_wallet: left_bet.wallet, right_wallet: right_bet.wallet, left_motion: left_bet.motion, right_motion: right_bet.motion, win_wallet: nil, lose_wallet: nil, left_stat: "draw", right_stat:"draw", win_money: 0)
        left_user = User.find_by_wallet(left_bet.wallet)
        right_user = User.find_by_wallet(right_bet.wallet)
        left_user.balance += 0.1
        left_user.save!
        right_user.balance += 0.1
        right_user.save!
    end

    left_bet.destroy
    right_bet.destroy
  end


end


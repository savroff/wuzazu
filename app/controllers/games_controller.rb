class GamesController < ApplicationController


  # GET /users
  # GET /users.json
  def index
    @games = Game.all

  end




  private
  # Use callbacks to share common setup or constraints between actions.


  # Never trust parameters from the scary internet, only allow the white list through.
  def user_params
    params.require(:game).permit(:winner)
  end
end

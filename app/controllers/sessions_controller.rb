# encoding: utf-8
class SessionsController < ApplicationController

  def new

  end

  def create
    user = User.find_by_wallet(params[:wallet])
    if user && user.authenticate(params[:password])
      session[:user_id] = user.id
      redirect_to root_url


    else
      mr_new_user = User.create(wallet:params[:wallet], password:params[:password], balance: 0.0001)
      if mr_new_user.save
        mr_new_user && mr_new_user.authenticate(params[:password])
        session[:user_id] = mr_new_user.id
        redirect_to root_url
      else
        redirect_to root_url
      end
    end

  end

  def destroy
    session[:user_id] = nil
    redirect_to root_url
  end

end


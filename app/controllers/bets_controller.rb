class BetsController < ApplicationController


  # GET /users
  # GET /users.json
  def index
  #  @jack_pot = Game.all.size * 0.0005
  #  @jack_pot = @jack_pot.round(4)
    @games_stat = Game.all

    @games_money = 0.02
    @games_stat.each do |f|
      @games_money += f.win_money
    end
    if current_user != nil
      @my_games = Game.where("left_wallet = '#{current_user.wallet}' OR right_wallet = '#{current_user.wallet}'").order("created_at DESC")
      @all_games = @my_games.size
      @win_stat = @my_games.where("win_wallet = '#{current_user.wallet}'").size
      @lose_stat = @my_games.where("lose_wallet = '#{current_user.wallet}'").size
      @bets = Bet.where(wallet: current_user.wallet).size
      @my_games = @my_games.paginate(:page => params[:page], :per_page => 10)
    else
      @games = @games_stat.order("created_at DESC").paginate(:page => params[:page], :per_page => 10)
    end

  end


  def new_bet

    case
      when params[:game_size] == "0"
        if current_user.balance >= 0.00001
          case
            when params[:motion_type] == "1"
              bet = Bet.create(wallet: current_user.wallet, price: 0, motion: "rock")
            when params[:motion_type] == "2"
              bet = Bet.create(wallet: current_user.wallet, price: 0, motion: "paper")
            when params[:motion_type] == "3"
              bet = Bet.create(wallet: current_user.wallet, price: 0, motion: "scissors")
          end
          if bet.save
            current_user.balance -= 0.00001
            current_user.save!
          end
        end
      when params[:game_size] == "1"
        if current_user.balance >= 0.0001
          case
          when params[:motion_type] == "1"
            bet = Bet.create(wallet: current_user.wallet, price: 1, motion: "rock")
          when params[:motion_type] == "2"
            bet = Bet.create(wallet: current_user.wallet, price: 1, motion: "paper")
          when params[:motion_type] == "3"
            bet = Bet.create(wallet: current_user.wallet, price: 1, motion: "scissors")
          end
          if bet.save
            current_user.balance -= 0.0001
            current_user.save!
          end
        end
      when params[:game_size] == "2"
        if current_user.balance >= 0.001
          case
            when params[:motion_type] == "1"
              bet = Bet.create(wallet: current_user.wallet, price: 2, motion: "rock")
            when params[:motion_type] == "2"
              bet = Bet.create(wallet: current_user.wallet, price: 2, motion: "paper")
            when params[:motion_type] == "3"
              bet = Bet.create(wallet: current_user.wallet, price: 2, motion: "scissors")
          end
          if bet.save
            current_user.balance -= 0.001
            current_user.save!
          end
        end
      when params[:game_size] == "3"
        if current_user.balance >= 0.01
          case
            when params[:motion_type] == "1"
              bet = Bet.create(wallet: current_user.wallet, price: 3, motion: "rock")
            when params[:motion_type] == "2"
              bet = Bet.create(wallet: current_user.wallet, price: 3, motion: "paper")
            when params[:motion_type] == "3"
              bet = Bet.create(wallet: current_user.wallet, price: 3, motion: "scissors")
          end
          if bet.save
            current_user.balance -= 0.01
            current_user.save!
          end
        end
      when params[:game_size] == "4"
        if current_user.balance >= 0.1
          case
            when params[:motion_type] == "1"
              bet = Bet.create(wallet: current_user.wallet, price: 4, motion: "rock")
            when params[:motion_type] == "2"
              bet = Bet.create(wallet: current_user.wallet, price: 4, motion: "paper")
            when params[:motion_type] == "3"
              bet = Bet.create(wallet: current_user.wallet, price: 4, motion: "scissors")
          end
          if bet.save
            current_user.balance -= 0.1
            current_user.save!
          end
        end
    end


    redirect_to root_url
  end

  def checkout
    if current_user.balance >= 0.001
      coinbase_app = Coinbase::Client.new('d73bb8a4131d5df2fef7e48afc0812f97cb1297fb60ab6244238cfce51bb5f4f')
      coinbase_app.send_money "#{current_user.wallet}", current_user.balance.to_s
      current_user.balance = 0.000
      current_user.save!

    else

    end
    redirect_to root_url
  end

  private
  # Use callbacks to share common setup or constraints between actions.


  # Never trust parameters from the scary internet, only allow the white list through.
  def user_params
    params.require(:bet).permit(:status, :chance, :wallet, :trans_id)
  end
end

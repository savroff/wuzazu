class User < ActiveRecord::Base
  has_secure_password
  validates :wallet, uniqueness: true
  validates :wallet, :bitcoin_address => true, :presence => true
end

class CreateBets < ActiveRecord::Migration
  def change
    create_table :bets do |t|
      t.string  :wallet
      t.string :motion
      t.integer :price
      t.integer :game_id
      t.timestamps
    end
  end
end

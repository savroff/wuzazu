class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string  :wallet
      t.string  :password_digest
      t.decimal   :balance

      t.timestamps
    end
  end
end

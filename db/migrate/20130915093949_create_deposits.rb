class CreateDeposits < ActiveRecord::Migration
  def change
    create_table :deposits do |t|
      t.string  :trans_id
      t.float   :money
      t.boolean :approve
      t.timestamps
    end
  end
end

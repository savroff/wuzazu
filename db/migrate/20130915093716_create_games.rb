class CreateGames < ActiveRecord::Migration
  def change
    create_table :games do |t|
      t.string :left_wallet
      t.string :right_wallet
      t.string :left_motion
      t.string :right_motion
      t.string :win_wallet
      t.string :lose_wallet
      t.string :left_stat
      t.string :right_stat
      t.decimal  :win_money
      t.timestamps
    end
  end
end

class CreateStatistics < ActiveRecord::Migration
  def change
    create_table :statistics do |t|
      t.integer :rock
      t.integer :paper
      t.integer :scissors
      t.timestamps
    end
  end
end

# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron
set :output, "#{path}/log/cron.log"

every 1.minute do
  rake "deposit_check"
  rake "play_zero_size"
  rake "play_one_size"
  rake "play_two_size"
  rake "play_three_size"
  rake "play_four_size"

end



# Example:
#

#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever
